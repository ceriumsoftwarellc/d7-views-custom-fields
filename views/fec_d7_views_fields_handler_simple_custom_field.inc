<?php
/**
 * @file
 * Definition of fec_d7_views_fields_handler_simple_custom_field
 */

/**
 * Provides a custom views field.
 */
class fec_d7_views_fields_handler_simple_custom_field extends views_handler_field {
    /**
     * Provide initial values for your custom form options
     *
     * Best practice is to prefix your form options witht he module name, to prevent collisions with other views fields.
     * If no form value is set in the options form, this value is used in the render step
     *
     * @return array
     */
    function option_definition() {
        $options = parent::option_definition();
        $options['fec_d7_views_fields_form_option'] = 'option_1';
        return $options;
    }

    function options_form(&$form, &$form_state) {
        // Load all of the other parts that go into this form
        parent::options_form($form, $form_state);

        // You may use anything from the Form API. In this case, we are using a select list
        $form['fec_d7_views_fields_form_option'] = array(
            '#type' => 'select',
            '#title' => t('Display option'),
            '#description' => t('This is an example of options which can be used during field rendering.'),
            '#options' => array('option_1' => 'Option 1', 'option_2' => 'Option 2'),
            '#default_value' => $this->options['fec_d7_views_fields_form_option'],
        );
    }

    function query() {
        $this->ensure_my_table();
        // do nothing -- to override the parent query.
    }

    function render($data) {

        /**
         * Generate output
         *
         * The return value of the function is markup you generate, so you may do this the Drupal way by creating a
         * render array and running drupal_render($array) at the end, or you can do it the "slacker way" and
         * concatenate HTML together. Which way to go will depend on your use case - here, we are showing which option
         * you had selected.
         */
        $output = '';

        $opts = array('option_1' => 'Option 1', 'option_2' => 'Option 2');
        $opt_selected = $this->options['fec_d7_views_fields_form_option'];

        // Render selected option, or show an 'invalid' message.
        $sel = isset($opts[$opt_selected]) ?  $opts[$opt_selected]: t('Invalid Value');
        $output .= t('You have selected: ') . $sel;


        return $output;
    }
}
