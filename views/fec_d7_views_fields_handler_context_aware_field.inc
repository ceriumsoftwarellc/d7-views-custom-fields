<?php
/**
 * @file
 * Definition of fec_d7_views_fields_handler_context_aware_field
 */

/**
 * Provides a custom views field.
 */
class fec_d7_views_fields_handler_context_aware_field extends views_handler_field {
    function option_definition() {
        $options = parent::option_definition();

        return $options;
    }

    /**
     * We are not including options_form as we are not modifying the form
     */
//  function options_form(&$form, &$form_state) {
//  }

    function query() {
        $this->ensure_my_table();
        // do nothing -- to override the parent query.
    }

    function render($data) {
        /**
         * Get the relevant nid
         *
         * If the base table is the node table, the row's base nid will be available under $data->nid
         *
         * Otherwise, if you specified a relationship in the field settings, you will find the nid under
         * $data->{$this->relationship . '_nid'}
         */
        $nid = $this->relationship ? $data->{$this->relationship . '_nid'} : $data->nid;
        if (!$nid) {
            return '';
        }

        /**
         * Generate output
         *
         * The return value of the function is markup you generate, so you may do this the Drupal way by creating a
         * render array and running drupal_render($array) at the end, or you can do it the "slacker way" and
         * concatenate HTML together. Which way to go will depend on your use case - here, we are showing a title
         * and NID.
         */
        $output = '';
        $node = node_load($nid);

        $output .= '<h4>' . t('About this node') . '</h4>';
        $output .= '<p>' . t('Title: ') . $node->title . '</p>';
        $output .= '<p>' . t('NID: ') . $node->nid . '</p>';


        return $output;
    }
}
