<?php

/**
 * @file
 * Views definitions for 'fec_d7_views_fields'
 */

/**
 * Implementation of hook_views_handlers() to register all of the basic handlers
 * views uses.
 */
function fec_d7_views_fields_views_handlers() {
  $handlers = array(
    'info' => array(
      'path' => drupal_get_path('module', 'fec_d7_views_fields'),
    ),
    'handlers' => array(
      // The name of my handler
      'fec_d7_views_fields_handler_simple_custom_field' => array(
        // The name of the handler we are extending.
        'parent' => 'views_handler_field',
      ),
      'fec_d7_views_fields_handler_context_aware_field' => array(
        // The name of the handler we are extending.
        'parent' => 'views_handler_field',
      ),
    ),
  );
  return $handlers;
}

/**
 * Implements hook_views_data().
 */
function fec_d7_views_fields_views_data() {
  $data = array();

  // Add custom field
  $data['global']['fec_d7_views_fields_handler_simple_custom_field'] = array(
    'title' => t('Flashpoint: Simple Custom Field'),
    'help' => t('Shows a simple custom field with a display option.'),
    'field' => array(
      'handler' => 'fec_d7_views_fields_handler_simple_custom_field',
    ),
  );
  $data['node']['fec_d7_views_fields_handler_context_aware_field'] = array(
    'title' => t('Flashpoint: Context-aware field'),
    'help' => t('Shows how to retrieve information from a node in the row.'),
    'field' => array(
      'handler' => 'fec_d7_views_fields_handler_context_aware_field',
    ),
  );

  return $data;
}

